  !Modelo de Hopfield
  
  program Hopfield
  
  implicit none
  integer, parameter		:: pr=kind(1.d0)
  integer, parameter		:: n=4000, pmax=n/5,deltap=n/100, tmax=20
  integer 			:: t, i, j, mu, h, p, num
  real(pr)			:: idum, q, m, M_medio
  integer, dimension (n,n)	:: w
  integer, dimension (n,pmax)	:: XSI
  integer, dimension (n)	:: s
  
  !Elegimos las memorias XSI (arreglo con p filas y n memorias)
  open(unit=11,file="hopfield3b.dat",status="replace")
  
  do p=deltap,pmax,deltap
    M_medio=0
    do num=1,10
      do i=1,n
	    do mu=1,p
	        q=rand (0) !numero entre 0 y 1 
	        if (q<=0.5) then
	            XSI(i,mu)=1
	        else
	            XSI(i,mu)=-1
	        end if
	    end do
      end do
      
      !Calculo w
      
      do i=1,n
	    do j=1,i-1
	      w(i,j)=0
	      do mu=1,p
	        w(i,j)=w(i,j)+XSI(i,mu)*XSI(j,mu)
	      end do
	      w(j,i)=w(i,j)
	    end do
	    w(i,i)=0
      end do
      
      !Elijo el imput: Una configuracion al azar
      
      s=XSI(:,1)
      
      do t=1,tmax
	    do i=1,nsale veinte pesos
	       h=0
	       do j=1,n
	         h=h+w(i,j)*s(j)
	       end do
	       if (h>0) then
	         s(i)=1
	       else
	         s(i)=-1
	       end if
	    end do
      end do
	  m=0._pr
	  do i=1,n
	    m=m+XSI(i,1)*s(i)
	  end do
	  m=m/n
	  M_medio=M_medio + m
    end do ! num
    write(11,*) p/float(n),M_medio/10.
    write(*,*) p/float(n),M_medio/10.

  end do ! p
  close(11)
  end program