# Hopfield

Implementación de una red de Hopfield de N = 500, 1000, 2000 y 4000 neuronas con dinámica determinista.

Para correr el programa:

>sudo apt-get install gfortran

>gfortran -o hopfield hopfield.f90

>./hopfield

En caso de querer variar la cantidad de neuronas utilizadas se deberá modificar la variable "n".